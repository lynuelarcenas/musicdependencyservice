﻿using System;
using System.IO;
using AVFoundation;
using Foundation;
using MusicDependencyService.iOS.DependencyService;
using MusicDependencyService.Utilities.DependencyServices;
using Xamarin.Forms;

[assembly: Dependency(typeof(AudioService))]

namespace MusicDependencyService.iOS.DependencyService
{
    public class AudioService :IPlayMusic
    {
        private AVAudioPlayer _audioPlayer = null;
        public Action OnFinishedPlaying { get; set; }
        public AudioService()
        {
        }

        public void Play(string fileName)
        {
            if (_audioPlayer != null)
            {
                _audioPlayer.FinishedPlaying -= Player_FinishedPlaying;
                _audioPlayer.Stop();
            }

            string localUrl = fileName;
            _audioPlayer = AVAudioPlayer.FromUrl(NSUrl.FromFilename(localUrl));
            _audioPlayer.FinishedPlaying += Player_FinishedPlaying;
            _audioPlayer.Play();
            //string sFilePath = NSBundle.MainBundle.PathForResource
            //(Path.GetFileNameWithoutExtension(fileName), Path.GetExtension(fileName));
            //var url = NSUrl.FromString(sFilePath);
            //var _player = AVAudioPlayer.FromUrl(url);
            //_player.FinishedPlaying += (object sender, AVStatusEventArgs e) =>
            //{
            //    _player = null;
            //};
            //_player.Play();
        }

        private void Player_FinishedPlaying(object sender, AVStatusEventArgs e)
        {
            OnFinishedPlaying?.Invoke();
        }

        public void Pause()
        {
            _audioPlayer?.Pause();
        }

        public void Play()
        {
            _audioPlayer?.Play();
        }
    }
}
