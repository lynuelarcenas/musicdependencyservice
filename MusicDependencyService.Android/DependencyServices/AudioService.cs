﻿using System;
using Android.Media;
using MusicDependencyService.Utilities.DependencyServices;
using Android.Content.Res;
using MusicDependencyService.Droid.DependencyServices;
using Xamarin.Forms;

[assembly: Dependency(typeof(AudioService))]
namespace MusicDependencyService.Droid.DependencyServices
{
    public class AudioService : IPlayMusic
    {
        public AudioService()
        {
        }

        //public Action OnFinishedPlaying { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        private MediaPlayer _mediaPlayer;
        public Action OnFinishedPlaying { get; set; }

        public void Play(string fileName)
        {
            if (_mediaPlayer != null)
            {
                _mediaPlayer.Completion -= MediaPlayer_Completion;
                _mediaPlayer.Stop();
            }

            var fullPath = fileName;

            Android.Content.Res.AssetFileDescriptor afd = null;

            try
            {
                afd = Forms.Context.Assets.OpenFd(fullPath);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error openfd: " + ex);
            }
            if (afd != null)
            {
                System.Diagnostics.Debug.WriteLine("Length " + afd.Length);
                if (_mediaPlayer == null)
                {
                    _mediaPlayer = new MediaPlayer();
                    _mediaPlayer.Prepared += (sender, args) =>
                    {
                        _mediaPlayer.Start();
                        _mediaPlayer.Completion += MediaPlayer_Completion;
                    };
                }

                _mediaPlayer.Reset();
                _mediaPlayer.SetVolume(1.0f, 1.0f);

                _mediaPlayer.SetDataSource(afd.FileDescriptor, afd.StartOffset, afd.Length);
                _mediaPlayer.PrepareAsync();
            }
        }

        void MediaPlayer_Completion(object sender, EventArgs e)
        {
            OnFinishedPlaying?.Invoke();
        }

        public void Pause()
        {
            _mediaPlayer?.Pause();
        }

        public void Play()
        {
            _mediaPlayer?.Start();
        }
    }
}
