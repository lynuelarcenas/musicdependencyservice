﻿using System;
using System.Collections.Generic;
using MusicDependencyService.Utilities.DependencyServices;
using MusicDependencyService.ViewModel;
using Xamarin.Forms;

namespace MusicDependencyService.Views
{
    public partial class AudioPlayerPage : ContentPage
    {
        public AudioPlayerPage()
        {
            InitializeComponent();
            BindingContext = new AudioPlayerViewModel(DependencyService.Get<IPlayMusic>());
        }

        void Handle_Clicked(object sender, System.EventArgs e)
        {
            DependencyService.Get<IPlayMusic>().Play("Californiacation.mp3");
        }
    }
}
