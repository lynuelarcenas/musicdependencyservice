﻿using System;
using System.ComponentModel;
using System.Windows.Input;
using MusicDependencyService.Utilities.DependencyServices;
using Xamarin.Forms;

namespace MusicDependencyService.ViewModel
{
    public class AudioPlayerViewModel : INotifyPropertyChanged
    {

        public AudioPlayerViewModel()
        {
        }

        private IPlayMusic _audioPlayer;

        private bool _isStopped;

        public event PropertyChangedEventHandler PropertyChanged;
        private string _commandText;
        private string _buttonColor;

        public string CommandText
        {
            get { return _commandText; }
            set
            {
                _commandText = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("CommandText"));
            }
        }

        public string ButtonColor
        {
            get { return _buttonColor; }
            set
            {
                _buttonColor = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("ButtonColor"));
            }
        }
       

        public AudioPlayerViewModel(IPlayMusic audioPlayer)
        {
            _audioPlayer = audioPlayer;
            _audioPlayer.OnFinishedPlaying = () => {
                _isStopped = true;
                CommandText = "Play";
                ButtonColor = "#34495e";

            };
            CommandText = "Play";
            ButtonColor = "#34495e";
            _isStopped = true;
        }

        private ICommand _playPauseCommand;

        public ICommand PlayPauseCommand
        {
            get
            {
                return _playPauseCommand ?? (_playPauseCommand = new Command(
                  (obj) =>
                  {
                      if (CommandText == "Play")
                      {
                          if (_isStopped)
                          {
                              _isStopped = false;
                              _audioPlayer.Play("CantStop.mp3");
                              
                          }
                          else
                          {
                              _audioPlayer.Play();
                          }
                        CommandText = "Pause";
                        ButtonColor = "#bdc3c7";
                      }
                      else
                      {
                          _audioPlayer.Pause();
                          CommandText = "Play";
                          ButtonColor = "#34495e";
                      }
                  }));
            }
        }
    }
}
