﻿using System;
namespace MusicDependencyService.Utilities.DependencyServices
{
    public interface IPlayMusic
    {
        void Play(string fileName);
        void Play();
        void Pause();
        Action OnFinishedPlaying { get; set; }
    
    }
}
